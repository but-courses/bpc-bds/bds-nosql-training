# check if elastic already started
until curl -X GET "$ELASTIC_URL/_cluster/health?wait_for_status=green&timeout=60s"
do
  echo "waiting for elasticsearch to start"
  sleep 10
done


curl -X PUT "$ELASTIC_URL/logstash-2015.05.18?pretty" -H 'Content-Type: application/json' -d'
{
  "mappings": {
    "log": {
      "properties": {
        "geo": {
          "properties": {
            "coordinates": {
              "type": "geo_point"
            }
          }
        }
      }
    }
  }
}
'

curl -X PUT "$ELASTIC_URL/logstash-2015.05.19?pretty" -H 'Content-Type: application/json' -d'
{
  "mappings": {
    "log": {
      "properties": {
        "geo": {
          "properties": {
            "coordinates": {
              "type": "geo_point"
            }
          }
        }
      }
    }
  }
}
'

curl -X PUT "$ELASTIC_URL/logstash-2015.05.20?pretty" -H 'Content-Type: application/json' -d'
{
  "mappings": {
    "log": {
      "properties": {
        "geo": {
          "properties": {
            "coordinates": {
              "type": "geo_point"
            }
          }
        }
      }
    }
  }
}
'

sleep 5

curl -H 'Content-Type: application/x-ndjson' -XPOST '$ELASTIC_URL/_bulk?pretty' --data-binary @logs.jsonl
