# Using Logs to Find SQL Injection Attack

Log files are very valuable information provided by the server. Almost all servers, services, and applications provide some kind of logging. A log file records individual events occurring in the system (e.g., servers, services, and applications) to record a sequence of activities from which we can find the problem. For example, one of the most common attacks on web applications may be SQL Injection.

## Use a log file to investigate SQL Injection attack

Launch `Git Bash` and open log file using `Vim` (beforehand you can open the `access.log` file in arbitrary editor to see how the log files looks like):
```shell
$ cd logs
$ vim access.log
```

The basic Vim commands are:
- i - insert mode
- x or Del - delete a character
- dd -- delete a line
- u - to undo the last the command
- U - to undo the whole line
- 0 (zero) to move to the start of the line.
- CTRL-R to redo
- Esc - cancel
- w: to save changes
- :wq - to save and exit
- :q! - to trash all changes
- **/<search_term>** - search and then cycle through matches with n and N

Use the search syntax to find SQL statement:

For example, lines containing the **SELECT** term can be found as follows:
```vim
/SELECT
```

This results in the following interesting records:
```
77.75.75.176 - - [25/May/2021:19:47:57 +0200] "GET / HTTP/1.1" 200 10439 "-" "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_3) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/26.0.1410.65 Safari/537.31" "-"
77.75.75.176 - - [25/May/2021:19:23:06 +0200] "GET / HTTP/1.1" 200 10439 "-" "Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.97 Mobile Safari/537.36 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)" "-"
77.75.75.176 - - [25/May/2021:18:39:22 +0200] "GET / HTTP/1.1" 200 10479 "-" "Mozilla/5.0 (Linux; Android 10; MAR-LX1A) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.210 Mobile Safari/537.36" "-"
77.75.75.176 - - [25/May/2021:20:57:40 +0200] "GET /index.php?category=1 UNION ALL SELECT *, NULL, NULL FROM users HTTP/1.1" 200 166 "-" "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4096.0 Safari/537.36" "-"
```

We see that the `IP 77.75.75.176` accessed the main page successfully. Let’s see what else the user with this IP address did (huh, `GET /index.php?category=1 UNION ALL SELECT *, NULL, NULL FROM users HTTP/1.1` isn't this a SQL injection?).

Exit `Vim` (enter ESC button if necessary):
```vim
:q!
```

Use `grep` command to filter the `access.log` with SQL statements terms:
```shell
$ grep -E 'SELECT|FROM|WHERE' access.log
```

Result contains only one line:
```
77.75.75.176 - - [25/May/2021:20:57:40 +0200] "GET /index.php?category=1 UNION ALL SELECT *, NULL, NULL FROM users HTTP/1.1" 200 166 "-" "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4096.0 Safari/537.36" "-"
```

You can also filter IP addresses with a `regular expression`:
```shell
$ grep -E 'SELECT|FROM|WHERE' access.log | egrep -o '([0-9]{1,3}\.){3}[0-9]{1,3}'
```


### Final remarks
- `Question 1`: Why we do not just open the file in the arbitrary text editor and press `CTRL+F` to find the SELECT clauses?
- `Answer 1`: To analyze the logs we usually need to connect to the server using `ssh` and then without any GUI download the logs using `scp` or open the log files using `vim` or similar tool.

For more advanced logs analysis we need more advanced tools (e.g., ELK or others). Here, we usually download the logs from the server using `scp` and insert them into the advanced solution to deeply analyze them -- or in real-time using advanced tools integration. The prerequisite for that is to format the logs in a suitable structure (e.g., JSON format).


## Analyze the result

The attacker accessed the main site using the GET parameter and injected a command to retrieve all users. It is common for an attacker to try different methods such as GET and POST for `SQL injection vulnerability`. SQL injection can happen through any mechanism where user data ends up directly in the query. This is why it's important to use prepared statements with placeholder values for all queries with user input.




